'use strict';
angular.module('app.services')
.factory('AllService', function($http, appConfig, $q) {
  var all = {
    sensors: [

    ],
    scenarios: [

    ]
  }
  return {
    getSensors: function() {
      var defer = $q.defer();

      if (all.sensors.length === 0) {
        return $http({
            url: appConfig.url + 'api/db/sensors',
            method: 'GET',
            headers: {
              'X-Api-Factory-Application-Id': '58555b414b7708199269e1e0',
              Authorization: 'Bearer 1bde1e7a5135c44c9573093c557ff2dfc59e4f73'
            }
          });
      } else {
        defer.resolve({
          data: {
            data: all.sensors
          }
        });
      }
      return defer.promise;
    },
    getScenarios: function() {
      var defer = $q.defer();

      if (all.scenarios.length === 0) {
        return $http({
            url: appConfig.url + 'api/db/scenarios',
            method: 'GET',
            headers: {
              'X-Api-Factory-Application-Id': '58555b414b7708199269e1e0',
              Authorization: 'Bearer 1bde1e7a5135c44c9573093c557ff2dfc59e4f73'
            }
          });
      } else {
        defer.resolve({
          data: {
            data: all.scenarios
          }
        });
      }
      return defer.promise;
    },
    keep: function(type, data) {
      all[type] = data;
    },
    getAll: function() {
      return all;
    }
  };
});
