'use strict';

angular.module('app', ['ui.router','app.services', 'app.controllers', 'ui.knob'])

// Просто так))
.run(function($rootScope, $state) {
  $rootScope.$on('$stateChangeStart', function (event, next) {
    $(document).foundation();
  });
})

// Наши стейты
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('Main', {
        url: '/Main',
        templateUrl: 'views/Main.html',
        controller: 'MainCtrl',
        resolve: {
          $title: function() { return 'Main';}
        }
    })
    .state('Scenarios', {
        url: '/Scenarios',
        templateUrl: 'views/Scenarios.html',
        controller: 'ScenariosCtrl',
        resolve: {
          $title: function() { return 'Scenarios';}
        }
    });
    // $urlRouterProvider.otherwise('Main');
})

// Конфиг с адресом сервера апи-фактори
.value('appConfig', {
  url: 'http://176.112.198.197/',
  ws: 'http://176.112.198.197:3000/'

});
