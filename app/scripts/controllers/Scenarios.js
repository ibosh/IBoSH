'use strict';
angular.module('app.controllers')
.controller('ScenariosCtrl', function($scope, $http, AllService) {
	$scope.all = {};
	AllService.getSensors().then(function(all) {
		$scope.all.sensors = all.data.data;
	});
	AllService.getScenarios().then(function(all) {
		$scope.all.scenarios = all.data.data;
		AllService.keep('scenarios', all.data);
	});

	$scope.showSensor = function (id) {
		console.log($scope.all.sensors[id].name);
	};

	$scope.runScenario = function (name) {
		$http({
			url: appConfig + 'api/script/sc' + name,
			method: 'GET'
		});
		console.log($scope.all.scenarios[id]);
	};

	$(document).foundation();
});
