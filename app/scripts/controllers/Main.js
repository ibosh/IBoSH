'use strict';
angular
  .module('app.controllers')
  .controller('MainCtrl', function($scope, AllService) {
    var watchers = [];
    var options = {
      skin: {
        type: 'tron',
        width: 5,
        color: '#494B52',
        spaceWidth: 3
      },
      size: 100,
      barColor: '#494B52',
      trackWidth: 0,
      barWidth: 20,
      textColor: '#494B52',
      step: 0.1,
      max: 1000
    };

    AllService.getSensors().then(function(all) {
      angular.forEach(all.data.data, function(s, i) {
        if (s.type === 'digit') {
          s.options = angular.copy(options);
          s.value = +s.value;
          if (s.max) {
            s.options.max = s.max;
          }
          if (s.min) {
            s.options.min = s.min;
          }
        } else {
          s.value = !!s.value;
        }

        $scope.sensors = all.data.data;

        watchers.push($scope.$watch('sensors['+ i + ']', sendToVova, true));
      });
    });

    function sendToVova(sensor) {
      socket.emit('sensorHasBeenChanged', {
        name: sensor.name,
        value: +sensor.value
      });
      smthChanged(sensor.name, sensor.value);
      console.log(sensor);
    };

    socket.on('sensorHasBeenChanged', function(sensor) {
      angular.forEach($scope.sensors, function(s) {
        if (s.name === sensor.name) {
          s.value = +sensor.value;
        }
      });
      smthChanged(sensor.name, sensor.value);
    });

    function smthChanged (name, value) {
      if (name.indexOf('Light') !== -1) {
        console.log(name);
        lightCtrl(name, value);
      } else if (name.indexOf('Door') !== -1){
        doorCtrl(name, value);
      }
    }

    $scope.$on('destroy', function() {
      for (var i = 0; i < watchers.length; i ++) {
        watchers[i]();
      }
    });


var svgLines = d3.select('.demoscene')
  .append('svg')
  .style("position", "absolute")
  .style("left", "0")
  .style("z-index", "0")
  .style("width", "100%")
  .style("height", "100%");
var x = 20;
while (x < 750) {
  svgLines
    .append('line')
    .style('stroke', 'grey')
    .style('stroke-width', 1)
    .attr('x1', x + 10)
    .attr('y1', 20)
    .attr('x2', x + 10)
    .attr('y2', 670);
  x += 10;
}

function doorCtrl (name, value) {
var door = d3.select('.' + name).select('rect');
console.log(door.attr('x') + ' ' + door.attr('y'));
d3.select('.' + name)
  .transition()
  .duration(750)
  .style('transform-origin', door.attr('x') + 'px ' + door.attr('y') + 'px')
  .style('transform', value ? 'rotate(-90deg)': 'rotate(0deg)')
  .select('rect')
  .style('fill', value ? 'green': 'red');
}

function lightCtrl (name, value) {
  d3.select('.' + name)
  .transition()
  .duration(750)
  .style('fill', value ? 'yellow': 'white');
}

});
